Menu Checker
============

This module is intended as a simple development and debugging tool.  Visit the
Performance page (Home > Administration > Configuration > Development) and
click on the "Clear all caches" button to view the site's menu array.
